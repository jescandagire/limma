# Limma

**Problem Statement**<br>
We are constantly hearing many people advising us that we should actually get into the farming practice but the we are actually provided with no information on how we can actually go about starting it.<br>
We even do not know where we can go to get the right seeds to use for planting, or even where to find someone actually experienced with the practice to provide us with the information we need so as to produce the quality products.<br>

**Solution**<br>
To develop a platform that can be used by anyone to start farming by providing the information required, from how to actually prepare the land, where to buy your seeds, 
how to grow the desired crop and also to locate the nearest farmers community for knowledge sharing. 

